﻿
<!--#echo json="package.json" key="name" underline="=" -->
@instaffogmbh/eslint-config-instaffo
====================================
<!--/#echo -->

<!--#echo json="package.json" key="description" -->
The eslint config we use at Instaffo.
<!--/#echo -->

* 📣 💼 💰 Looking for a tech job?
  Try our [reverse recruiting service](https://instaffo.com/).



Usage
-----

1. `npm install --save-dev @instaffogmbh/eslint-config-instaffo`
1. In your module's `package.json`, add

    ```json
      "eslintConfig": { "extends": "@instaffogmbh/eslint-config-instaffo" },
      "scripts": {
        "justlint": "eslint --ext .mjs,.js . && echo '+OK 👍'",
        "lint": "eslint --ext .mjs,.js --fix . && echo '+OK 👍'",
      },
    ```

    * Unfortunately, even in eslint v6.3.0, `--ext` still
      [is the only way][why--ext] to configure which extensions to scan.

1. `npm run lint`




<!--#toc stop="scan" -->


&nbsp;

  [why--ext]: https://eslint.org/docs/user-guide/configuring#specifying-file-extensions-to-lint


License
-------
<!--#echo json="package.json" key=".license" -->
MIT
<!--/#echo -->
