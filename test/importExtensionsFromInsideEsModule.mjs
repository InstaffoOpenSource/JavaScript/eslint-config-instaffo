import test from 'p-tape'

import canImportCJS from './import_fext/cjs_into_esm'
import canImportESM from './import_fext/esm_into_esm'
import guessImportFext from './import_fext/noext_into_esm'


test('Can import CJS', async(t) => {
  t.plan(1)
  t.deepEqual(canImportCJS, { imported: 'cjs', into: 'esm' })
})

test('Can import ESM', async(t) => {
  t.plan(1)
  t.deepEqual(canImportESM, { imported: 'esm', into: 'esm' })
})

test('Guess import filename extension', async(t) => {
  t.plan(1)
  t.deepEqual(guessImportFext, { imported: 'esm', into: 'esm' })
})
