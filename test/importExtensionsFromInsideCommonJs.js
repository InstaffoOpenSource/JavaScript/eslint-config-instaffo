const test = require('p-tape')

const canImportCJS = require('./import_fext/cjs_into_cjs')
const canImportCJSwithFext = require('./import_fext/cjs_into_cjs.js')
const guessImportFext = require('./import_fext/noext_into_cjs')

test('Can import CJS', async(t) => {
  t.plan(2)
  t.deepEqual(canImportCJS, { imported: 'cjs', into: 'cjs' })
  t.deepEqual(canImportCJSwithFext, { imported: 'cjs', into: 'cjs' })
})

test('Guess import filename extension', async(t) => {
  t.plan(1)
  t.deepEqual(guessImportFext, { imported: 'cjs', into: 'cjs' })
})
