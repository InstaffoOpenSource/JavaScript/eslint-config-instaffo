const exbrace = require('brace-expansion')
const flatten = require('flatten')

const devStuffDirNames = [
  'build',
  'debug',
  'test',
]
const extraDepsOpts = {
  devDependencies: flatten([
    '…*.test.{m,}js',
    ...devStuffDirNames.map(dn => `…${dn}/…*.{m,}js`),
  ].map(p => p.replace(/…/g, '{,**/}')).map(exbrace)),
}

module.exports = {
  extends: [
    'eslint-config-airbnb-base',
  ].map(require.resolve),
  // ^-- Resolving our deps absolves dependent modules from declaring
  //     our deps as their own. (cf. eslint-config-airbnb's index.js)

  rules: {
    'max-len': ['error', 120],
    'comma-dangle': ['error', {
      arrays: 'always-multiline',
      objects: 'always-multiline',
      imports: 'always-multiline',
      exports: 'always-multiline',
      functions: 'never',
    }],
    'function-paren-newline': 0,
    'object-curly-newline': 0,
    'space-before-function-paren': ['error', 'never'],
    'class-methods-use-this': 0,
    quotes: ['error', 'single', {
      allowTemplateLiterals: true,
      avoidEscape: true,
    }],
    semi: ['error', 'never'],
    'no-alert': 1,
    'no-console': 1,
    'prefer-destructuring': ['error', {
      array: false,
      object: true,
    }, {
      enforceForRenamedProperties: false,
    }],
    'import/no-extraneous-dependencies': ['error', extraDepsOpts],
  },
}
